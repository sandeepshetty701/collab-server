
const mongoose = require('mongoose');
const db_host = process.env.DB_HOST;
const db_name = process.env.DB_NAME;
const db_port = process.env.DB_PORT;
const db_user = process.env.DB_USER;
const db_pass = process.env.DB_PASS;

mongoose.Promise = global.Promise;
if(db_user && db_pass){
    mongoose.connect(`mongodb://${db_user}:${db_pass}@${db_host}:${db_port}/${db_name}`, { useNewUrlParser: true });
}else{
    mongoose.connect(`mongodb://${db_host}:${db_port}/${db_name}`, { useNewUrlParser: true });
}
// require('../models/workFlowModel');
require('../models/userModel');
require('../models/userTokenModel');
require('../models/userModel');
require('../models/postModel');
require('../models/projectModel');
// require('../models/jobModel');
// require('../models/missionPathModel');

module.exports = {
    models:{
        // WorkFlow: mongoose.model('WorkFlow'),
        User: mongoose.model('User'),
        UserToken: mongoose.model('UserToken'),
        Post: mongoose.model('Post'),
        Project: mongoose.model('Project')
        // Jobs: mongoose.model('Job'),
        // MissionPath : mongoose.model('MissionPath')
    }
}
