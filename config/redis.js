var redis = require("redis");
var post_client = redis.createClient({ host: process.env.REDIS_HOST, port: process.env.REDIS_PORT, db: process.env.REDIS_POST_DB});
var logger = require('../utils/logger').getLogger('Config Redis');
 
 
post_client.on("error", function (err) {
    logger.error("Error " + err);
});

module.exports = {
    clients: {
        post_client: post_client
    }
}