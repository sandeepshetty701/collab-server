// import entire SDK
const AWS = require('aws-sdk');

AWS.config = {
    "accessKeyId": process.env.AWS_ACCESS_KEY,
    "secretAccessKey": process.env.AWS_SECRET_ACCESS,
    "region": process.env.AWS_REGION
};
const s3 = new AWS.S3({
    "apiVersions": process.env.AWS_S3_VERSION,
    "endpoint": process.env.AWS_S3_ENDPOINT
});




module.exports = {
    s3: s3,
    s3_stream_handler: require('s3-upload-stream')(s3)
}