#!/bin/bash

docker() {
#    if [ ! -e ~/.docker/config.json ]; then
        command docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
#	fi
	command docker "$@"
}

docker_build_push() {
	echo Building docker image...
	docker build -t $CI_REGISTRY_IMAGE:$1 .

	echo Pushing docker image...
	docker push $CI_REGISTRY_IMAGE:$1
}

docker_tag_push() {
	echo Pulling docker image...
	docker pull $CI_REGISTRY_IMAGE:$1 >/dev/null

	echo Tagging docker image...
	docker tag $CI_REGISTRY_IMAGE:$1 $CI_REGISTRY_IMAGE:$2

	echo Pushing docker image...
	docker push $CI_REGISTRY_IMAGE:$2
}

get_var() {
    local tmp
    tmp=${BUILD_ENV}_$1
    printf "%s\n" "${!tmp}"
}

setup_env() {
    BUILD_ENV=$1
    DOTENVFILE=.env

    cp .env-dist $DOTENVFILE

    cat >> $DOTENVFILE <<- EOL
SENDER_PASSWORD=$(eval echo "\'\${${BUILD_ENV}_SENDER_PASSWORD}\'")
MONGODB_URI=$(eval echo "\'\${${BUILD_ENV}_MONGODB_URI}\'")
FP_SECRET=$(eval echo "\'\${${BUILD_ENV}_FP_SECRET}\'")
JWT_SECRET=$(eval echo "\'\${${BUILD_ENV}_JWT_SECRET}\'")
HTTPS_PASSPHRASE=$(eval echo "\'\${${BUILD_ENV}_HTTPS_PASSPHRASE}\'")
EOL
}
