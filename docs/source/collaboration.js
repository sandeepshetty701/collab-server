/**
 *
 * @api {get} /api/v1/collaboration/post List Posts
 * @apiGroup Post-Management
 * @apiVersion  1.0.0
 * @apiHeader {String} x-auth-token Users unique token.
 *
 * @apiHeader {String} access-key Users unique access-key.
 * @apiHeader {String} access-key Users unique access-key.
 * @apiHeader {String} access-key Users unique access-key.
 * 
 * @apiParam  {uuid}   followedBy
 * @apiParam  {uuid}   user
 * @apiParam  {uuid}   project
 * @apiParam  {String} createdAt
 * @apiParam  {String} updatedAt
 * @apiParamExample  {Object} Request-Example:
 * https://3rdi-staging.xyzinnotech.com/api/v1/collaboration/post?user=5cad9440ddad89601aade4ef&sort=likes&order=dsc
 * @apiParamExample  {Object} Request-Example:
 * https://3rdi-staging.xyzinnotech.com/api/v1/collaboration/post?followedBy=5cad9440ddad89601aade4ef
 * @apiParamExample  {Object} Request-Example:
 * https://3rdi-staging.xyzinnotech.com/api/v1/collaboration/post?repliesTo=5cad9440ddad89601aade4ef
 * @apiParamExample  {Object} Request-Example:
 * https://3rdi-staging.xyzinnotech.com/api/v1/collaboration/post?relavantTo=5cad9440ddad89601aade4ef
 * 
 * @apiParamExample  {json/urlencoded} Request-Example:
 *  {
 *    "user":"5cad9440ddad89601aade53f",
 *  }
 * @apiParamExample  {json/urlencoded} Request-Example:
 *  {
 *    "sort": "likes",
 *    "order": "dsc"
 *  }
 * 
 * @apiSuccessExample {json} Success-Response:
 * [
    {
        "followers": [],
        "likes": 55,
        "unlikes": 0,
        "attachments": [],
        "_id": "5cad9440ddad89601aade54f",
        "owner": "5cad9440ddad89601aade53f",
        "project": "5abf81b88c0d217615448d01",
        "post_type": "corrupti",
        "title": "Recusandae neque",
        "body": "Odio rerum itaque animi voluptate debitis.",
        "created_at": "2019-04-10T06:59:12.972Z",
        "updated_at": "2019-04-10T06:59:12.972Z",
        "__v": 0
    },
    {
        "followers": [],
        "likes": 54,
        "unlikes": 0,
        "attachments": [],
        "_id": "5cad9440ddad89601aade551",
        "owner": "5cad9440ddad89601aade549",
        "project": "5abf81b88c0d217615448d01",
        "post_type": "fugit",
        "title": "Sunt et nulla",
        "body": "Rem minima et fugiat ullam.",
        "created_at": "2019-04-10T06:59:12.972Z",
        "updated_at": "2019-04-10T06:59:12.972Z",
        "__v": 0
    }
]
 *
 * @apiErrorExample {json}  Server Error
 *    HTTP/1.1 500 Internal Server Error
 *    {
 *       status: "error",
 *       message: "Internal Server Error"
 *    }
 * @apiErrorExample {json} Validation Error
 *    HTTP/1.1 403
 *    {
 *       status: "error",
 *       message: "**************"
 *    }
 */

/**
 *
 * @api {patch} /api/v1/collaboration/post/:postId Update Post
 * @apiGroup Post-Management
 * @apiVersion  1.0.0
* @apiHeader {String} x-auth-token Users unique token.

 *
 * @apiParam  {uuid} :postId mission's id 
 * @apiParamExample  {Object} Request-Example:
 * https://3rdi-staging.xyzinnotech.com/api/v1/collaboration/post/5cad9440ddad89601aade505
 
* @apiParamExample  {json} Request-Example:
 {
  "title": "New Title",
  "body": "New body",
  "attachments": ["attachment", "attachment"]
 }
 * 
 * @apiSuccessExample {json} Success-Response:
 * {
    "followers": [],
    "likes": 38,
    "unlikes": 0,
    "attachments": [],
    "_id": "5cad9440ddad89601aade505",
    "owner": "5cad9440ddad89601aade4ef",
    "project": "5abf81b88c0d217615448d01",
    "post_type": "beatae",
    "title": "New Title",
    "body": "Ut omnis veniam sapiente dolores vitae quaerat qui.",
    "created_at": "2019-04-10T06:59:12.237Z",
    "updated_at": "2019-04-11T06:40:43.532Z",
    "__v": 0
 }
 *
 * @apiErrorExample {json}  Server Error
 *    HTTP/1.1 500 Internal Server Error
 *    {
 *       status: "error",
 *       message: "Internal Server Error"
 *    }
 * @apiErrorExample {json} Validation Error
 *    HTTP/1.1 403
 *    {
 *       status: "error",
 *       message: "**************"
 *    }
 */




 /**
 *
 * @api {POST} /api/v1/collaboration/post Create Post
 * @apiGroup Post-Management
 * @apiVersion  1.0.0
 *
 * @apiHeader {String} x-auth-token - SessionId of user.
 * 
 * @apiParam  {String}   title - title of the post or reply
 * @apiParam  {String}   text_content - text contents 
 * @apiParam  {String}   project - Project ID to which this post or reply is related.
 * @apiParam  {String}   post_type - `reply` or `post`.
 * @apiParam  {String}   parent - null or a valid post id
 * @apiParam  {String}   tags - comma seperated userids without space where each userid is taken as a tag
 * @apiParam  {String}   selectedUsers - comma seperated userids without space
 * @apiParam  {String}   visibility - all-org-users or all-proj-users or selected-users
 * @apiParam  {File}     file - any number of files.
 *
 * @apiSuccessExample {json} Success-Response:
 * {
    "followers": [],
    "likes": 0,
    "unlikes": 0,
    "attachments": [
        "https://test-xyz-collaboration.s3.ap-south-1.amazonaws.com/5ccac07ea5f79e620deed481/attachments/DJI_0002.JPG",
        "https://test-xyz-collaboration.s3.ap-south-1.amazonaws.com/5ccac07ea5f79e620deed481/attachments/DJI_0001.JPG"
    ],
    "tags": [],
    "replies": [],
    "_id": "5ccac07ea5f79e620deed481",
    "project": "5abf81b88c0d217615448d01",
    "post_type": "post",
    "title": "Test Abil",
    "text_content": "Dummy Text",
    "owner": "5ca1de531df8e04ccb3bea8a",
    "created_at": "2019-05-02T10:03:42.867Z",
    "updated_at": "2019-05-02T10:04:20.034Z",
    "__v": 0
}
 * @apiErrorExample {json} Error-Response:
 * HTTP/1.1 500 Internal Server Error
 * {
    "status": "error",
    "message": "Internal Server Error",
    "info": {
        "post_id": "53372323bdy27328"
    }
 }
 * @apiErrorExample {json} Error-Response:
 * HTTP/1.1 409 Conflict
 * {
    "status": "error",
    "message": "Conflicted Resource",
    "info": {
        "post_id": "53372323bdy27328"
    }
 }
 * @apiErrorExample {json} Error-Response:
 * HTTP/1.1 403 Missing Fields
 * {
    "status": "error",
    "message": "Please Provide the Required Fields",
    "info": {
        "post_id": "53372323bdy27328"
    }
 }
 * @apiErrorExample {json} Error-Response:
 * HTTP/1.1 403 Missing Post ID
 * {
    "status": "error",
    "message": "Missing Post ID Header",
    "info": {
        "post_id": "53372323bdy27328"
    }
 }
 * @apiErrorExample {json} Error-Response:
 * HTTP/1.1 403 Invalid Tags Field
 * {
    "status": "error",
    "message": "Invalid Tags Field",
    "info": {
        "post_id": "53372323bdy27328"
    }
 }
 * @apiErrorExample {json} Error-Response:
 * HTTP/1.1 403 Conflict
 * {
    "status": "error",
    "message": "Please Provide the Required Fields",
    "info": {
        "post_id": "53372323bdy27328"
    }
 }
 * @apiErrorExample {json} Error-Response:
 * HTTP/1.1 403 Conflict
 * {
    "status": "error",
    "message": "No token provided",
    "info": {
        "post_id": "53372323bdy27328"
    }
 }
 * @apiErrorExample {json} Error-Response:
 * HTTP/1.1 401 Unauthorized
 * {
    "status": "error",
    "message": "Unauthorized"
 }
 *
 */

 /**
 *
 * @api {POST} /api/v1/collaboration/post/attachment Add Attachments To Post
 * @apiGroup Post-Management
 * @apiVersion  1.0.0
 *
 * @apiHeader {String} x-auth-token - SessionId of user.
 * @apiHeader {String} x-userid - Email ID of user.
 * @apiHeader {String} x-requestid - UUID send along with each request.
 * @apiHeader {Number} x-files-attached - Number of files send along with the request. value > 0.
 * @apiHeader {String} x-post-id - UUID created for a specific post.
 * @apiHeader {String} content-type - mutlipart/form-data.
 *
 * @apiParam  {File}     any name - any number of files.
 *
 * @apiSuccessExample {json} Success-Response:
 * {
    "status": "success",
    "message": "Attachment have been added successfully",
    "info": {
        "post_id": "53372323bdy27328"
    }
 }
 * @apiErrorExample {json} Error-Response:
 * HTTP/1.1 500 Internal Server Error
 * {
    "status": "error",
    "message": "Internal Server Error",
    "info": {
        "post_id": "53372323bdy27328"
    }
 }
 * @apiErrorExample {json} Error-Response:
 * HTTP/1.1 403 Missing Header
 * {
    "status": "error",
    "message": "Missing Post ID Header",
    "info": {
        "post_id": "53372323bdy27328"
    }
 }
 * @apiErrorExample {json} Error-Response:
 * HTTP/1.1 403 Zero Valued File Attach Header
 * {
    "status": "error",
    "message": "Please Provide Non-Zero File Attach Header",
    "info": {
        "post_id": "53372323bdy27328"
    }
 }
 * @apiErrorExample {json} Error-Response:
 * HTTP/1.1 403 No Auth Token 
 * {
    "status": "error",
    "message": "No token provided",
    "info": {
        "post_id": "53372323bdy27328"
    }
 }
 * @apiErrorExample {json} Error-Response:
 * HTTP/1.1 401 Unauthorized
 * {
    "status": "error",
    "message": "Unauthorized"
 }
 *
 */

 /**
 *
 * @api {DELETE} /api/v1/collaboration/post Delete Post
 * @apiGroup Post-Management
 * @apiVersion  1.0.0
 *
 * @apiHeader {String} x-auth-token - SessionId of user.
 * @apiHeader {String} x-userid - Email ID of user.
 * @apiHeader {String} x-requestid - UUID send along with each request.
 * @apiHeader {String} content-type - `application/json` or `application/x-www-form-urlencoded`.
 *
 * @apiParam  {String} post_id - Post Object ID(will be a UUID).
 *
 * @apiSuccessExample {json} Success-Response:
 * {
    "status": "success",
    "message": "Post have been deleted successfully",
    "info": {
        "post_id": "53372323bdy27328"
    }
 }
 * @apiErrorExample {json} Error-Response:
 * HTTP/1.1 500 Internal Server Error
 * {
    "status": "error",
    "message": "Internal Server Error",
    "info": {
        "post_id": "53372323bdy27328"
    }
 }
 * @apiErrorExample {json} Error-Response:
 * HTTP/1.1 403 Missing Header
 * {
    "status": "error",
    "message": "Empty PostId",
    "info": {
        "post_id": "53372323bdy27328"
    }
 }
 * @apiErrorExample {json} Error-Response:
 * HTTP/1.1 403 Pattern or Non Existent PostId
 * {
    "status": "error",
    "message": "Invalid PostId",
    "info": {
        "post_id": "53372323bdy27328"
    }
 }
 * @apiErrorExample {json} Error-Response:
 * HTTP/1.1 403 No Auth Token 
 * {
    "status": "error",
    "message": "No token provided",
    "info": {
        "post_id": "53372323bdy27328"
    }
 }
 * @apiErrorExample {json} Error-Response:
 * HTTP/1.1 401 Unauthorized
 * {
    "status": "error",
    "message": "Unauthorized"
 }
 *
 */


 /**
 *
 * @api {patch} /api/collaboration/post/like/:postId Like Post
 * @apiGroup Post-Management
 * @apiVersion  1.0.0
 * @apiHeader {String} x-auth-token Users unique token.
 *
 * @apiParam  {uuid} :postId mission's id 
 * @apiParamExample  {Object} Request-Example:
 * https://3rdi-staging.xyzinnotech.com/api/v1/collaboration/post/like/5cad9440ddad89601aade505
 
 * @apiSuccessExample {json} Success-Response:
 * {
    "followers": [],
    "likes": 39,
    "unlikes": 0,
    "attachments": [],
    "_id": "5cad9440ddad89601aade505",
    "owner": "5cad9440ddad89601aade4ef",
    "project": "5abf81b88c0d217615448d01",
    "post_type": "beatae",
    "title": "New Title",
    "body": "Ut omnis veniam sapiente dolores vitae quaerat qui.",
    "created_at": "2019-04-10T06:59:12.237Z",
    "updated_at": "2019-04-11T06:40:43.532Z",
    "__v": 0
 }
 *
 * @apiErrorExample {json}  Server Error
 *    HTTP/1.1 500 Internal Server Error
 *    {
 *       status: "error",
 *       message: "Internal Server Error"
 *    }
 * @apiErrorExample {json} Validation Error
 *    HTTP/1.1 403
 *    {
 *       status: "error",
 *       message: "**************"
 *    }
 */

  /**
 *
 * @api {patch} /api/collaboration/post/dislike/:postId Dislike Post
 * @apiGroup Post-Management
 * @apiVersion  1.0.0
 * @apiHeader {String} x-auth-token Users unique token.

 *
 * @apiParam  {uuid} :postId mission's id 
 * @apiParamExample  {Object} Request-Example:
 * https://3rdi-staging.xyzinnotech.com/api/v1/collaboration/post/dislike/5cad9440ddad89601aade505
 
 * @apiSuccessExample {json} Success-Response:
 * {
    "followers": [],
    "likes": 38,
    "unlikes": 1,
    "attachments": [],
    "_id": "5cad9440ddad89601aade505",
    "owner": "5cad9440ddad89601aade4ef",
    "project": "5abf81b88c0d217615448d01",
    "post_type": "beatae",
    "title": "New Title",
    "body": "Ut omnis veniam sapiente dolores vitae quaerat qui.",
    "created_at": "2019-04-10T06:59:12.237Z",
    "updated_at": "2019-04-11T06:40:43.532Z",
    "__v": 0
 }
 *
 * @apiErrorExample {json}  Server Error
 *    HTTP/1.1 500 Internal Server Error
 *    {
 *       status: "error",
 *       message: "Internal Server Error"
 *    }
 * @apiErrorExample {json} Validation Error
 *    HTTP/1.1 403
 *    {
 *       status: "error",
 *       message: "**************"
 *    }
 */

 /**
 *
 * @api {patch} /api/v1/collaboration/post/follow/:postId Follow Post
 * @apiGroup Post-Management
 * @apiVersion  1.0.0
 * @apiHeader {String} x-auth-token Users unique token.
 *
 *
 * @apiParam  {uuid} :postId mission's id 
 * @apiParamExample  {Object} Request-Example:
 * https://3rdi-staging.xyzinnotech.com/api/v1/collaboration/post/follow/5cad9440ddad89601aade505
 
 * @apiSuccessExample {json} Success-Response:
 * {
    "followers": ["5cad9440ddad89601aade4hi"],
    "likes": 39,
    "unlikes": 0,
    "attachments": [],
    "_id": "5cad9440ddad89601aade505",
    "owner": "5cad9440ddad89601aade4ef",
    "project": "5abf81b88c0d217615448d01",
    "post_type": "beatae",
    "title": "New Title",
    "body": "Ut omnis veniam sapiente dolores vitae quaerat qui.",
    "created_at": "2019-04-10T06:59:12.237Z",
    "updated_at": "2019-04-11T06:40:43.532Z",
    "__v": 0
 }
 *
 * @apiErrorExample {json}  Server Error
 *    HTTP/1.1 500 Internal Server Error
 *    {
 *       status: "error",
 *       message: "Internal Server Error"
 *    }
 * @apiErrorExample {json} Validation Error
 *    HTTP/1.1 403
 *    {
 *       status: "error",
 *       message: "**************"
 *    }
 */

  /**
 *
 * @api {patch} /api/collaboration/post/unfollow/:postId Unfollow Post
 * @apiGroup Post-Management
 * @apiVersion  1.0.0
 * @apiHeader {String} x-auth-token Users unique token.
 *
 * @apiParam  {uuid} :postId mission's id 
 * @apiParamExample  {Object} Request-Example:
 * https://3rdi-staging.xyzinnotech.com/api/v1/collaboration/post/unfollow/5cad9440ddad89601aade505
 
 * @apiSuccessExample {json} Success-Response:
 * {
    "followers": [],
    "likes": 38,
    "unlikes": 1,
    "attachments": [],
    "_id": "5cad9440ddad89601aade505",
    "owner": "5cad9440ddad89601aade4ef",
    "project": "5abf81b88c0d217615448d01",
    "post_type": "beatae",
    "title": "New Title",
    "body": "Ut omnis veniam sapiente dolores vitae quaerat qui.",
    "created_at": "2019-04-10T06:59:12.237Z",
    "updated_at": "2019-04-11T06:40:43.532Z",
    "__v": 0
 }
 *
 * @apiErrorExample {json}  Server Error
 *    HTTP/1.1 500 Internal Server Error
 *    {
 *       status: "error",
 *       message: "Internal Server Error"
 *    }
 * @apiErrorExample {json} Validation Error
 *    HTTP/1.1 403
 *    {
 *       status: "error",
 *       message: "**************"
 *    }
 */

