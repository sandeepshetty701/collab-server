// Post routes
const express = require('express');

const router = express.Router({mergeParams: true});

const postController = require('../controllers/postController');

//list all posts
router.get('/', postController.listPosts);

//update post
router.patch('/:postId', postController.updatePost);

router.post('/attachment', postController.uploadAttachment);
router.post('/', postController.createPostV2);
router.delete('/', postController.deletePost);
//like post
router.patch('/like/:postId', postController.likePost);

//dislike post
router.patch('/dislike/:postId', postController.dislikePost)

//follow post
router.patch('/follow/:postId', postController.followPost)

//unfollow post
router.patch('/unfollow/:postId', postController.unfollowPost)

router.all('*', function(req, res){
    res.status(400)
    res.json({
        status: 'error',
        message: 'URI Not Found'
    });
});

module.exports = router;