'use strict';

var express = require('express');
var router = express.Router();

var middlewares = require('../middlewares');

var postRoutes = require('./postRoutes');

router.use(middlewares.authFunc.checkUserToken);
router.use('/post', postRoutes);


router.all('*', function(req, res){
    res.status(400)
    res.send('Invalid route');
});

module.exports = router;