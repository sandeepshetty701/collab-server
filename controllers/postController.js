// Logic for Post Management

'use strict';

var logger = require('../utils/logger').getLogger('Post Controller');
const M = '[Controller][Post]';
const blogic = require('../lib/blogic');
const upload = require('../lib/uploads');

var listPosts = async function (req, res) {
    try {
        var resObj = await blogic.post.listPosts(req.headers['x-auth-token'], req.user._id.toString(), req.query);
        resObj.status = 'success';
        logger.info("Request fullfilled - 200 OK");
        return res.status(200).json(resObj);
    } catch (err) {
        if (err.name == "DB_Error") {
            var resObj = {
                "status": "Error",
                "message": err.message,
                "description": err.description
            };
            logger.error(resObj)
            return res.status(500).json(resObj);
        } else if (err.name == "NoPostEntries") {

            return res.status(200).json([]);
        } else {
            resObj = {
                "status": "Error",
                "message": "Internal Server Error",
                "description": err
            };
            logger.error(resObj);
            return res.status(500).json(resObj);
        }
    }
}

var createPostV2 = async function (req, res) {
    try {
        var resObj = await blogic.post.createPostV2(req);
        resObj.status = 'success';
        logger.info("Request fullfilled - 200 OK");
        return res.status(200).json(resObj);
    } catch (err) {
        if (err.name == "DB_Error") {
            var resObj = {
                "status": "Error",
                "message": err.message,
                "description": err.description
            };
            logger.error(resObj)
            return res.status(500).json(resObj);
        } else {
            resObj = {
                "status": "Error",
                "message": "Internal Server Error",
                "description": err
            };
            logger.error(resObj);
            return res.status(500).json(resObj);
        }
    }
}

var updatePost = async function (req, res) {
    try {
        var resObj = await blogic.post.updatePost(req.params, req.body);
        resObj.status = 'success';
        logger.info("Request fullfilled - 200 OK");
        return res.status(200).json(resObj);
    } catch (err) {
        if (err.name == "DB_Error") {
            var resObj = {
                "status": "Error",
                "message": err.message,
                "description": err.description
            };
            logger.error(resObj)
            return res.status(500).json(resObj);

        } else if (err.name == "PostNotFound") {
            resObj = {
                "status": "Error",
                "message": err.message
            };
            logger.error(resObj)
            return res.status(400).json(resObj);
        } else if (err.name == "DBCastError") {
            resObj = {
                "status": "Error",
                "message": err.message,
                "description": err.description
            };
            logger.error(resObj)
            return res.status(400).json(resObj);
        } else {
            resObj = {
                "status": "Error",
                "message": "Internal Server Error",
                "description": err
            };
            logger.error(resObj);
            return res.status(500).json(resObj);
        }
    }
}

var createPost = async function (req, res) {
    const S = req.request_info + M + '[createPost]';
    try {
        upload(req, blogic.post.checkCreatePostRequest, blogic.post.uploadAttachment, blogic.post.createPost, () => {}, (err, result) => {
            if (err) {
                logger.error(S, err, 'api failed');
                var err_data = {
                    status: "error",
                    info: result
                };
                if(err == 'Conflicted Resource'){
                    return res.status(409).json({
                        status: 'error',
                        message: err,
                        info: result
                    });
                }else
                if (typeof err == 'string') {
                    return res.status(403).json({
                        status: 'error',
                        message: err,
                        info: result
                    });

                } else {
                    return res.status(500).json({
                        status: 'error',
                        message: err.message || 'Internal Server Error',
                        info: result
                    });
                }

            } else {
                logger.info(S, result, 'api successfull');

                return res.status(200).json({
                    status: "success",
                    message: "Post have been successfully created",
                    info: result
                });
            }

        });
    } catch (err) {
        logger.error(S, err, 'api failed');
        return res.status(500).json({
            status: 'error',
            message: err.message || 'Internal Server Error'
        });
    }
}

var uploadAttachment = async function (req, res) {
    const S = req.request_info + M + '[createPost]';
    try {
        upload(req, () => {
            return true;
        }, blogic.post.uploadAttachment, () => {}, () => {}, (err, result) => {
            if (err) {
                logger.error(S, err, 'api failed');
                var err_data = {
                    status: "error",
                    info: result
                };
                if (typeof err == 'string') {
                    return res.status(403).json({
                        status: 'error',
                        message: err,
                        info: result
                    });

                } else {
                    return res.status(500).json({
                        status: 'error',
                        message: err.message || 'Internal Server Error',
                        info: result
                    });
                }

            } else {
                logger.info(S, result, 'api successfull');

                return res.status(200).json({
                    status: "success",
                    message: "Attachment have been added successfully",
                    info: result
                });
            }

        });
    } catch (err) {
        logger.error(S, err, 'api failed');
        return res.status(500).json({
            status: 'error',
            message: err.message || 'Internal Server Error'
        });
    }
}


var deletePost = async function (req, res) {
    const S = req.request_info + M + '[deletePost]';
    try {
        var post_id = req.body.post_id;
        var result = {
            post_id: post_id
        };
        if (!post_id) {
            return res.status(403).json({
                status: "error",
                message: "Empty PostId"
            });
        }
        await blogic.post.deletePost(req.request_info, post_id);
        return res.status(200).json({
            status: "success",
            message: "Post have been deleted successfully",
            info: result
        });
    } catch (err) {
        logger.error(S, err, 'api failed');
        var err_data = {
            status: "error",
            info: result
        };
        if (typeof err == 'string') {
            return res.status(403).json({
                status: 'error',
                message: err,
                info: result
            });

        } else {
            return res.status(500).json({
                status: 'error',
                message: err.message || 'Internal Server Error',
                info: result
            });
        }

    }

}

var likePost = async function (req, res) {
    try {
        var resObj = await blogic.post.likePost(req.params);
        resObj.status = 'success';
        logger.info("Request fullfilled - 200 OK");
        return res.status(200).json(resObj);
    } catch (err) {
        if (err.name == "DB_Error") {
            var resObj = {
                "status": "Error",
                "message": err.message,
                "description": err.description
            };
            logger.error(resObj)
            return res.status(500).json(resObj);

        } else if (err.name == "PostNotFound") {
            resObj = {
                "status": "Error",
                "message": err.message
            };
            logger.error(resObj)
            return res.status(400).json(resObj);
        } else if (err.name == "DBCastError") {
            resObj = {
                "status": "Error",
                "message": err.message,
                "description": err.description
            };
            logger.error(resObj)
            return res.status(400).json(resObj);
        } else {
            resObj = {
                "status": "Error",
                "message": "Internal Server Error",
                "description": err
            };
            logger.error(resObj);
            return res.status(500).json(resObj);
        }
    }
}

var dislikePost = async function (req, res) {
    try {
        var resObj = await blogic.post.dislikePost(req.params);
        resObj.status = 'success';
        logger.info("Request fullfilled - 200 OK");
        return res.status(200).json(resObj);
    } catch (err) {
        if (err.name == "DB_Error") {
            var resObj = {
                "status": "Error",
                "message": err.message,
                "description": err.description
            };
            logger.error(resObj)
            return res.status(500).json(resObj);

        } else if (err.name == "PostNotFound") {
            resObj = {
                "status": "Error",
                "message": err.message
            };
            logger.error(resObj)
            return res.status(400).json(resObj);
        } else if (err.name == "DBCastError") {
            resObj = {
                "status": "Error",
                "message": err.message,
                "description": err.description
            };
            logger.error(resObj)
            return res.status(400).json(resObj);
        } else {
            resObj = {
                "status": "Error",
                "message": "Internal Server Error",
                "description": err
            };
            logger.error(resObj);
            return res.status(500).json(resObj);
        }
    }
}

var followPost = async function (req, res) {
    try {
        var resObj = await blogic.post.followPost(req.params, req.user);
        resObj.status = 'success';
        logger.info("Request fullfilled - 200 OK");
        return res.status(200).json(resObj);
    } catch (err) {
        if (err.name == "DB_Error") {
            var resObj = {
                "status": "Error",
                "message": err.message,
                "description": err.description
            };
            logger.error(resObj)
            return res.status(500).json(resObj);

        } else if (err.name == "PostNotFound") {
            resObj = {
                "status": "Error",
                "message": err.message
            };
            logger.error(resObj)
            return res.status(400).json(resObj);
        } else if (err.name == "DBCastError") {
            resObj = {
                "status": "Error",
                "message": err.message,
                "description": err.description
            };
            logger.error(resObj)
            return res.status(400).json(resObj);
        } else {
            resObj = {
                "status": "Error",
                "message": "Internal Server Error",
                "description": err
            };
            logger.error(resObj);
            return res.status(500).json(resObj);
        }
    }
}

var unfollowPost = async function (req, res) {
    try {
        var resObj = await blogic.post.unfollowPost(req.params, req.user);
        resObj.status = 'success';
        logger.info("Request fullfilled - 200 OK");
        return res.status(200).json(resObj);
    } catch (err) {
        if (err.name == "DB_Error") {
            var resObj = {
                "status": "Error",
                "message": err.message,
                "description": err.description
            };
            logger.error(resObj)
            return res.status(500).json(resObj);

        } else if (err.name == "PostNotFound") {
            resObj = {
                "status": "Error",
                "message": err.message
            };
            logger.error(resObj)
            return res.status(400).json(resObj);
        } else if (err.name == "DBCastError") {
            resObj = {
                "status": "Error",
                "message": err.message,
                "description": err.description
            };
            logger.error(resObj)
            return res.status(400).json(resObj);
        } else {
            resObj = {
                "status": "Error",
                "message": "Internal Server Error",
                "description": err
            };
            logger.error(resObj);
            return res.status(500).json(resObj);
        }
    }
}

module.exports = {
    createPost: createPost,
    createPostV2: createPostV2,
    listPosts: listPosts,
    updatePost: updatePost,
    uploadAttachment: uploadAttachment,
    deletePost: deletePost,
    likePost: likePost,
    dislikePost: dislikePost,
    followPost: followPost,
    unfollowPost: unfollowPost
}