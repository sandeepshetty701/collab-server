'use strict';

var logger = require('../utils/logger').getLogger('Middleware Common');

function addRequestInfo(req, res, next){
    const S = '[addRequestInfo]';
    console.log(req.headers, 'headers')
    if(!req.headers['x-requestid'] || !req.headers['x-userid'] ){
        logger.error(S, req.headers);
        return res.status(403).json({
            status: 'error',
            message: 'Please Provide The Required Headers'
        })
    }else{
        req.request_info = req.headers['x-requestid'] + req.headers['x-userid'];
        next();
    }
}

module.exports = {
    addRequestInfo: addRequestInfo
}
