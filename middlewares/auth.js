'use strict';

var logger = require('../utils/logger').getLogger('Middleware Auth');

const storage = require('../lib/storage');

var checkUserToken = async function (req, res, next) {
    var auth_token = req.headers['x-auth-token'];

    // decode token
    if (auth_token) {

        try{
            // get the corresponding user details for the auth-token
            var token_info = await storage.mongoDB.getUserTokenFromDB(auth_token);
                // check if the token is valid
            if (token_info) {
                // setting the user decoded token details
                req.user = token_info.user;
                req.decoded = token_info;
                next();
            } else {
                logger.error(req.headers, 'token not found');
                return res.status(401).json({ status: 'error', message: 'Unauthorized' });
            }
        }catch(err){
            logger.error(req.headers, err);

            return res.status(500).json({ status: 'error', message: 'Internal Server Error'});
        }
    } else {

        // if there is no token
        // return an error
        logger.error(req.headers, 'token not provided from client side');

        return res.status(403).send({
            status: 'success',
            message: 'No token provided.'
        });

    }
}




module.exports = {
    checkUserToken: checkUserToken
}