const redis = require('../../config/redis');
const post_client = redis.clients.post_client;

const M = '[Lib][Storage][AWS]';

var logger = require('../../utils/logger').getLogger('');

module.exports = {
    getFileList: function (request_info, post_id) {
        const S = request_info + M + '[getFileList]';
        return new Promise((resolve, reject) => {
            try {
                post_client.smembers(`post_id:${post_id}`, (err, data) => {
                    if (err) {
                        logger.error(S, err, data);
                        return reject(err);
                    } else if (data) {
                        logger.info(S, 'list of attachments', post_id, data);
                        return resolve(data);
                    } else {
                        logger.info(S, 'no attachments found', post_id, data);
                        return resolve(false);
                    }
                });
            } catch (err) {
                logger.error(S, err);
                return reject(err);
            }
        });
    },
    createFileList: function (request_info, post_id, file_path) {
        const S = request_info + M + '[createFileList]';
        return new Promise((resolve, reject) => {
            try {
                post_client.sadd(`post_id:${post_id}`, file_path, (err) => {
                    if (err) {
                        logger.error(S, err);
                        return reject(err);
                    } else {
                        logger.info(S, 'attachment added to cache', post_id, file_path);
                        return resolve('success');
                    }
                });
            } catch (err) {
                logger.error(S, err);
                return reject(err);
            }
        });
    },
    clearFileList: function (request_info, post_id) {
        const S = request_info + M + '[clearFileList]';
        return new Promise((resolve, reject) => {
            try {
                post_client.del(`post_id:${post_id}`, (err) => {
                    if (err) {
                        logger.error(S, err);
                        return reject(err);
                    } else {
                        logger.info(S, 'attachments deleted from cache', post_id);
                        return resolve('success');
                    }
                });
            } catch (err) {
                logger.error(S, err);
                return reject(err);
            }
        });
    }

}