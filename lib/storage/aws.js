require('dotenv').config();

const aws = require('../../config/aws');
const fs = require('fs');

const M = '[Lib][Storage][AWS]';

var logger = require('../../utils/logger').getLogger('');
const async = require('async');


module.exports = {


    streamFilesToS3: function (request_info, bucket, file, s3_path) {
        const S = request_info + M + '[streamFilesToS3]';

        return new Promise((resolve, reject) => {
            try {
                const params = {
                    Bucket: bucket,
                    Key: s3_path
                };

                var upload = aws.s3_stream_handler.upload(params);

                // Optional configuration
                upload.maxPartSize(20971520); // 20 MB
                upload.concurrentParts(5);

                // Handle errors.
                upload.on('error', function (error) {
                    logger.error(S, params, error);
                    return reject(error);
                });

                // Part info
                upload.on('part', function (details) {
                    logger.debug(S, params, details);
                });

                // Upload completed
                upload.on('uploaded', function (details) {
                    logger.info(S, "uploaded", params, details);
                    return resolve('success');
                });

                file.pipe(upload);
            } catch (err) {
                logger.error(S, s3_path, err);
                return reject(err);
            }
        });
    },

    uploadFileToS3: function (bucket, file, s3_path) {
        logger.info("storage->aws->uploadFileToS3")
        return new Promise((resolve, reject) => {
            fs.readFile(file.path, (err, data) => {
                if(err){
                    logger.error("File reading error", err);
                    reject(err);
                }
                try {
                    const params = {
                        Bucket: bucket,
                        Key: s3_path,
                        Body: data
                    };

                    aws.s3.upload(params, function(err, data) {
                        if (err) throw err
                        logger.info(`File uploaded successfully at ${data.Location}`)
                        fs.unlinkSync(file.path)
                        return resolve(data.Location)
                    });

                } catch (err) {
                    logger.error(s3_path, err);
                    return reject(err);
                }
            });
        });
    },

    deleteFromS3: function (request_info, bucket, s3_path) {
        const S = request_info + M + '[deleteFromS3]';

        return new Promise(async (resolve, reject) => {
            try {
                var params = {
                    Bucket: bucket, 
                    Prefix: s3_path,
                    MaxKeys: 200
                };
                aws.s3.listObjectsV2(params, function(err, data) {
                    if (err){
                        logger.error(S, bucket, s3_path, err); // an error occurred
                        return resolve('success');
                    }else{

                        logger.info(S, bucket, s3_path, data.Contents? data.Contents.length: 0); 
                        if(data.Contents && data.Contents.length){
                            async.each(data.Contents, (s3_object, cbDeep) =>{
                                const param = {
                                    Bucket: bucket,
                                    Key: s3_object.Key
                                };
                                aws.s3.deleteObject(param, function (err, data) {
                                    if (err) {
                                        cbDeep(err);   
                                    } else {
                                        cbDeep(null);
                                    }
                                });
                            }, (err)=>{
                                if (err) {
                                    logger.error(S, bucket, s3_path, err, 's3 error'); // an error occurred
                                    return reject(err);
                                } else {
                                    logger.info(S, bucket, s3_path, 'success'); // successful response
                                    return resolve("success");
            
                                }
                            });
                        }
                    }
                });
            } catch (err) {
                logger.error(S, bucket, s3_path, err, 'catch error'); // an error occurred
                return reject(err);
            }
        });

    }
}