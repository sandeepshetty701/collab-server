module.exports = {
    mongoDB: require('./mongodb'),
    aws: require('./aws'),
    redis: require('./redis') 
}