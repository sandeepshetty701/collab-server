var lodash = require('lodash');

var mongodb = require('../../config/mongodb');
// var logger = require('../../utils/logger').getLogger('Storage Mongodb');

//models
const M = '[Lib][Storage][MongoDB]';
var UserToken = mongodb.models.UserToken;
var Post = mongodb.models.Post;
var logger = require('../../utils/logger').getLogger('');



function getUserTokenFromDB(token) {
    return new Promise(async (resolve, reject) => {
        try {
            var userToken = await UserToken.find({
                token: token
            }).populate({
                path: 'user',
                select: 'email fullName _id organisationId'
            }).exec();
            if (lodash.isEmpty(userToken)) {
                logger.info('User Token Not Present', token);
                return resolve(null);
            } else {
                logger.info('User Token Found', token);
                return resolve(userToken[0]);
            }
        } catch (err) {
            logger.error(err, token);
            return reject(err);
        }
    });    
}

function getPostData(request_info, queryObj) {
    const S = request_info + M + '[getPostData]';
    return new Promise(async (resolve, reject) => {
        try {
            var post = await Post.find(queryObj).populate({
                path: 'owner',
                select: 'email fullName _id organisationId'
            }).exec();
            if (lodash.isEmpty(post)) {
                logger.error(S, 'Post Data Not Present', post, queryObj);
                return resolve(null);
            } else {
                logger.info(S, 'Post Data Found', post, queryObj);
                return resolve(post[0]);
            }
        } catch (err) {
            logger.error(S, err, queryObj);
            return reject(err);
        }
    });
}

function getPostsFromDB(dbQuery, sortQuery) {
    return new Promise(async (resolve, reject) => {
        try {
            var posts = await Post.find(dbQuery)
            .populate('owner','-password')
            .populate('tags')
            .populate('followers')
            .populate('replies')
            .sort(sortQuery)
            .exec()
            if (lodash.isEmpty(posts)) {
                return resolve(null);
            } else {
                return resolve(posts);
            }
        } catch (err) {
            if (err.name == "CastError") {
                reject({
                    "name": "DBCastError",
                    "description": err,
                    "message": "Cast Error - Check db query params"
                });
            } else {
                reject({
                    "name": "DB_Error",
                    "description": err,
                    "message": "Internal Server Error"
                });
            }
        }
    });
}

function createPostInDB(user, fields) {
    return new Promise(async (resolve, reject) => {
        if (user == null){
            reject({
                "name": "Authorization Error",
                "description": err,
                "message": "Token does not match any user"
            });
        }
        try {
          var postBody = lodash.pick(fields, ['parent', 'project', 'post_type', 'title', 'text_content', 'tags', 'selectedUsers', 'visibility']);
          if ("tags" in postBody){
            var tagArry = postBody.tags.split(",")
            postBody.tags = tagArry;
          }
          if ("selectedUsers" in postBody){
            var selectedUsersArry = postBody.selectedUsers.split(",")
            postBody.selectedUsers = selectedUsersArry;
          }
          
          if(postBody.post_type == "reply"){
            var parentPost = await Post.findById(postBody.parent).populate('owner','-password').exec();

            var post = new Post(postBody);
            post.owner = user;
            post.visibility = parentPost.visibility
            var createdPost = await post.save()
            
            parentPost.replies.push(createdPost._id);
            parentPost.save();
          }else {
            var post = new Post(postBody);
            post.owner = user;
            var createdPost = await post.save()
          }
          resolve(createdPost)
        } catch (err) {
            if (err.name == "CastError") {
                reject({
                    "name": "DBCastError",
                    "description": err,
                    "message": "Cast Error - Check db query params"
                });
            } else {
                reject({
                    "name": "DB_Error",
                    "description": err,
                    "message": "Internal Server Error"
                });
            }
        }
    });
}

function createPostData(request_info, postData) {
    const S = request_info + M + '[createPostData]';
    return new Promise(async (resolve, reject) => {
        try {
            var _id = postData._id;
            delete postData._id;
            Post.findOneAndUpdate({
                _id: _id
            }, postData, {
                upsert: true,
                // setDefaultsOnInsert: true,
                new: true
            }, function (err, post_info) {
                if (err) {
                    logger.error(S + 'error post data', err, postData);
                    return reject(err);
                } else {
                    logger.info(S + 'post info ---->', postData);
                    return resolve(post_info);
                }
            });
        } catch (err) {
            logger.error(S, err, postData);
            return reject(err);
        }
    });
}

function updatePostInDB(postId, postData) {
    return new Promise(async (resolve, reject) => {
        try {
            
            var post = await Post.findById(postId).exec();
            if (lodash.isEmpty(post)) {
                reject({
                    "name": "PostNotFound",
                    "message": "Cant update post as post with given id does not exist"
                });
            }
            
            post = await Post.findByIdAndUpdate(postId, postData, { new: true });
            return resolve(post);
        } catch (err) {
            if (err.name == "CastError") {
                reject({
                    "name": "DBCastError",
                    "description": err,
                    "message": "Cast Error - Check post id"
                });
            } else {
                reject({
                    "name": "DB_Error",
                    "description": err,
                    "message": "Internal Server Error"
                });
            }
        }
    });
}

function deletePostData(request_info, queryObj) {
    const S = request_info + M + '[deletePostData]';

    return new Promise(async (resolve, reject) => {
        try {
            await Post.deleteOne(queryObj).exec();
            return resolve('success');
        } catch (err) {
            if (err.name == "CastError") {
                reject({
                    "name": "DBCastError",
                    "description": err,
                    "message": "Cast Error - Check post id"
                });
            } else {
                reject({
                    "name": "DB_Error",
                    "description": err,
                    "message": "Internal Server Error"
                });
            }
        }
    });
}

module.exports = {
    getUserTokenFromDB: getUserTokenFromDB,
    getPostData: getPostData,
    createPostData: createPostData,
    createPostInDB: createPostInDB,
    getPostsFromDB: getPostsFromDB,
    updatePostInDB: updatePostInDB,
    deletePostData: deletePostData
}