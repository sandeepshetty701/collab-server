var storage = require('./storage');
var async = require('async');
var lodash = require('lodash');
var mongoose = require('mongoose');
var request = require('request');
var logger = require('../utils/logger').getLogger('Blogic');
const M = '[lib][blogic]';
var formidable = require('formidable');

var post = {
    screenPosts: function(token, posts, requestedBy){
        return new Promise(async (resolve, reject) => {
            screenedPosts = []
            for (i in posts){
                var cp = posts[i]
                if (!("visibility" in cp)){
                    continue
                }else{
                    var cpVisibility = cp.visibility
                }

                if(cpVisibility == "all-org-users"){
                    var cpProject = cp.project.toString()
                    var cpProjectOrg = ''
                    var cpProjectOrgUsers = []
                    cpProjectOrgUsers.push(cp.owner._id.toString())

                    var projectOptions = {
                        url: 'https://projects.api.staging.xyzinnotech.com/api/v1/projects/'+cpProject,
                        headers: {
                        'x-auth-token': token
                        }
                    };

                    try {
                        logger.debug("Api for getting project details,", projectOptions)
                        await request(projectOptions, (err, res, body) => {
                            if (err) { return console.log(err); }
                            var body = JSON.parse(body)
                            if("organisationId" in body){
                                cpProjectOrg = body.organisationId.toString()
                            }else{
                                logger.error("Project of the post doesnot have any organisation")
                                reject({
                                    "status": "Error",
                                    "message": "Screening Failed, Post's project does not have an organisation"
                                });
                            }
                            //Todo
                            //Use XYZ Org ID in the future for Orgless Projects
                            // }else{
                            //     cpProjectOrg = 'xyzorgid'
                            // }
                        });

                    }catch(err){
                        logger.error("Api for getting project details failed,", err)
                        reject({
                            "status": "Error",
                            "message": "Screening Failed, Could not get organisation for project details via projects api",
                            "Error": err
                        });
                    }

                    var options = {
                        url: 'https://auth.api.staging.xyzinnotech.com/api/v1/auth/user/all/?organisation='+cpProjectOrg,
                        headers: {
                        'x-auth-token': token
                        }
                    };

                    try{
                        logger.debug("Api for getting org users,", options)
                        await request(options, (err, res, body) => {
                            if (err) { return console.log(err); }
                            var body = JSON.parse(body)
                            if(body.users.length > 0){
                                for(i in body.users){
                                    cpProjectOrgUsers.push(body.users[i]._id.toString())
                                }
                            }
                        });
                    }catch(err){
                        logger.error("Api for getting org users failed,", err)
                        reject({
                            "status": "Error",
                            "message": "Screening failed, Could not get users of organisation using auth apis",
                            "Error": err
                        });
                    }

                    if (cpProjectOrgUsers.includes(requestedBy)){
                        screenedPosts.push(cp)
                    }

                }

                if(cpVisibility == "all-proj-users"){
                    var cpProject = cp.project.toString()
                    var cpProjectUsers = []
                    cpProjectUsers.push(cp.owner._id.toString())

                    var options = {
                        url: 'https://auth.api.staging.xyzinnotech.com/api/v1/auth/user/all/?project='+cpProject,
                        headers: {
                        'x-auth-token': token
                        }
                    };

                    try{
                        logger.debug("Api for getting project users,", options)
                        await request(options, (err, res, body) => {
                            if (err) { return console.log(err); }
                            var body = JSON.parse(body)
                            if(body.users.length > 0){
                                for(i in body.users){
                                    cpProjectUsers.push(body.users[i].user.toString())
                                }
                            }
                        });
                    }catch(err){
                        logger.error("Api for getting project users failed,", err)
                        reject({
                            "status": "Error",
                            "message": "Screening failed, Could not get users of project using auth apis",
                            "Error": err
                        });
                    }

                    if (cpProjectUsers.includes(requestedBy)){
                        screenedPosts.push(cp)
                    }
                }

                if(cpVisibility == "selected-users"){
                    var selecUsers = []
                    //Making array of strings from array of objects
                    for(j in cp.selectedUsers){
                        selecUsers.push(cp.selectedUsers[j].toString())
                    }
                    if(selecUsers.includes(requestedBy)){
                        screenedPosts.push(cp)
                    }
                }
            }
            resolve(screenedPosts)
        });
    },

    listPosts: function (token, requestedBy, query) {
        logger.info("IN --> blogic/post/listPosts");
        return new Promise(async (resolve, reject) => {

            try {
                var postParameters = lodash.pick(query, ['user', 'project','createdAt','updatedAt', 'sort', 'order', 'followedBy', 'repliesTo', 'relavantTo']);
                var dbQuery = {};
                var sortQuery = {};

                if ("relavantTo" in postParameters){
                    if ("sort" in postParameters) {
                        var ord = 1
                        if ("order" in postParameters && postParameters.order == "dsc") {
                            ord = -1
                        }
                        var sorting = postParameters.sort
                        if (sorting == "likes") {
                            sortQuery["likes"] = ord
                        } else if (sorting == "createdAt") {
                            sortQuery["created_at"] = ord
                        } else if (sorting == "updatedAt") {
                            sortQuery["updated_at"] = ord
                        } else {
                            reject({
                                "name": "InvalidSortKey",
                                "message": "Check if valid sort key is passed"
                            });
                        }
                    }
                    dbQuery["owner"] = mongoose.Types.ObjectId(postParameters.relavantTo);
                    dbQuery["post_type"] = "post"
                    logger.debug("find and sort query objects", dbQuery, sortQuery);
                    var posts = await storage.mongoDB.getPostsFromDB(dbQuery, sortQuery);
                    if (posts.length < 1){
                        posts = []
                    }
                    dbTagsQuery = {};
                    dbTagsQuery["tags"] = mongoose.Types.ObjectId(postParameters.relavantTo);
                    dbTagsQuery["post_type"] = "post"
                    logger.debug("find and sort query objects", dbTagsQuery, sortQuery);
                    var morePosts = await storage.mongoDB.getPostsFromDB(dbTagsQuery, sortQuery);
                    posts = posts.concat(morePosts);
                    posts = await this.screenPosts(token, posts, requestedBy)
                    resolve(posts)
                }

                if ("repliesTo" in postParameters){
                    dbQuery["parent"] = mongoose.Types.ObjectId(postParameters.repliesTo);
                    dbQuery["post_type"] = "reply"
                    if ("sort" in postParameters) {
                        var ord = 1
                        if ("order" in postParameters && postParameters.order == "dsc") {
                            ord = -1
                        }
                        var sorting = postParameters.sort
                        if (sorting == "likes") {
                            sortQuery["likes"] = ord
                        } else if (sorting == "createdAt") {
                            sortQuery["created_at"] = ord
                        } else if (sorting == "updatedAt") {
                            sortQuery["updated_at"] = ord
                        } else {
                            reject({
                                "name": "InvalidSortKey",
                                "message": "Check if valid sort key is passed"
                            });
                        }
                    }
                    logger.debug("find and sort query objects", dbQuery, sortQuery);
                    var posts = await storage.mongoDB.getPostsFromDB(dbQuery, sortQuery);
                    posts = await this.screenPosts(token, posts, requestedBy)
                    resolve(posts)
                }

                if ("followedBy" in postParameters){
                    dbQuery["followers"] = mongoose.Types.ObjectId(postParameters.followedBy);
                    dbQuery["post_type"] = "post"
                    if ("sort" in postParameters) {
                        var ord = 1
                        if ("order" in postParameters && postParameters.order == "dsc") {
                            ord = -1
                        }
                        var sorting = postParameters.sort
                        if (sorting == "likes") {
                            sortQuery["likes"] = ord
                        } else if (sorting == "createdAt") {
                            sortQuery["created_at"] = ord
                        } else if (sorting == "updatedAt") {
                            sortQuery["updated_at"] = ord
                        } else {
                            reject({
                                "name": "InvalidSortKey",
                                "message": "Check if valid sort key is passed"
                            });
                        }
                    }
                    logger.debug("find and sort query objects", dbQuery, sortQuery);
                    var posts = await storage.mongoDB.getPostsFromDB(dbQuery, sortQuery);
                    posts = await this.screenPosts(token, posts, requestedBy)
                    resolve(posts)
                }

                if ("user" in postParameters) {
                    dbQuery["owner"] = mongoose.Types.ObjectId(postParameters.user);
                } else if ("project" in postParameters) {
                    dbQuery["project"] = mongoose.Types.ObjectId(postParameters.project);
                } else if ("createdAt" in postParameters) {
                    dbQuery["created_at"] = Date.parse(postParameters.createdAt);
                } else if ("updatedAt" in postParameters) {
                    dbQuery["updated_at"] = Date.parse(postParameters.updatedAt);
                }

                if ("sort" in postParameters) {
                    var ord = 1
                    if ("order" in postParameters && postParameters.order == "dsc") {
                        ord = -1
                    }
                    var sorting = postParameters.sort
                    if (sorting == "likes") {
                        sortQuery["likes"] = ord
                    } else if (sorting == "createdAt") {
                        sortQuery["created_at"] = ord
                    } else if (sorting == "updatedAt") {
                        sortQuery["updated_at"] = ord
                    } else {
                        reject({
                            "name": "InvalidSortKey",
                            "message": "Check if valid sort key is passed"
                        });
                    }
                }
                dbQuery["post_type"] = "post"
                logger.debug("find and sort query objects", dbQuery, sortQuery);
                var posts = await storage.mongoDB.getPostsFromDB(dbQuery, sortQuery);
                posts = await this.screenPosts(token, posts, requestedBy)
                resolve(posts)

            } catch (err) {
                logger.error("Could not list posts,", err)
                reject(err)
            }

        });
    },

    createPostV2: function (req) {
        logger.info("IN --> blogic/post/createPostV2");
        return new Promise( (resolve, reject) => {
            try{
                var form = new formidable.IncomingForm(),
                files = []
                fields ={}
                form.on('field', function(name, value) {
                    fields[name] = value;
                });
                form.on('file', function(name, file) {
                    files.push([name, file]);
                })
                form.on('end', async function() {
                    var post = await storage.mongoDB.createPostInDB(req.user, fields)
                    var postData = {};
                    postData.attachments = []
                    try {
                        for (file in files){
                            var file_path = post._id+'/attachments/'+files[file][1].name
                            var s3Loc = await storage.aws.uploadFileToS3(process.env.BLOG_ATTACHMENT_BUCKET, files[file][1], file_path);
                            postData.attachments.push(s3Loc)
                        }    
                        var updatedPost = await storage.mongoDB.updatePostInDB(post._id, postData);
                        resolve(updatedPost)
                    } catch (err) {
                        await storage.mongoDB.deletePostData("[createPostV2]", {_id: mongoose.Types.ObjectId(post._id)})
                        logger.error("Could not upload attachments", err)
                        reject(err)
                    }
                });
                form.parse(req);
            } catch (err) {
                logger.error("Could not create post", err)
                reject(err)
            }
        });
    },

    updatePost: function (params, body) {
        logger.info("IN --> blogic/post/updatePost");
        return new Promise(async (resolve, reject) => {
            try {
                var postId = params.postId
                var postData = lodash.pick(body, ['title', 'body', 'attachments']);
                logger.debug("postId and postData:", postId, postData)
                var data = await storage.mongoDB.updatePostInDB(postId, postData);
                resolve(data);
            } catch (err) {
                logger.error("Could not update post:", err)
                reject(err)
            }
        });
    },

    checkCreatePostRequest: function (req, body) {
        const S = req.request_info + M + '[checkPostRequest]';

        return (body.project && body.title && body.post_type && body.text_content && body.hasOwnProperty('tag_length') && (!body.tag_length || (body.tag_length && body.tags.length && (body.tag_length == body.tags.length))));
        // return (body.project && body.title && body.post_type && body.text_content && body.hasOwnProperty('tag_length') && (!body.tag_length || (body.tag_length && body.tags.length && (body.tag_length == body.tags.length))));
    },

    createPost: function (req, body) {
        const request_info = req.request_info;
        const S = request_info + M + '[createPost]';
        return new Promise((resolve, reject) => {
            try {
                process.nextTick(async () => {
                    var postData = lodash.pick(body, ['project', 'post_type', 'title', 'text_content', 'owner', 'parent', '_id', 'tags']);
                    
                    logger.debug('date --- cp before getfilelist', new Date())

                    var attachments = await storage.redis.getFileList(request_info, body._id);

                    console.log('attachments ', attachments);
                    if (attachments && attachments.length) {
                        body.attachments.push.apply(body.attachments, attachments);
                    }
                    postData.$addToSet= {
                        attachments: body.attachments
                    };
                    var post_info = await storage.mongoDB.getPostData(request_info, {
                        _id: body._id
                    });
                    if (post_info) {
                        logger.error(S, 'conflicted resource', body);
                        return reject('Conflicted Resource');
                    } else {
                        await storage.mongoDB.createPostData(request_info, postData);
                        await storage.redis.clearFileList(request_info, body._id);
                        logger.info(S, 'success', body);
                        return resolve('success');
                    }

                });
            } catch (err) {
                logger.error(S, err, body);
                return reject(err);
            }
        });
    },
    uploadAttachment: function (req, file, body) {
        const request_info = req.request_info;
        const S = request_info + M + '[uploadAttachment]';
        return new Promise((resolve, reject) => {
            try {
                process.nextTick(async () => {

                    // var exists = await storage.redis.checkIfAlreadyUploadFile(request_info, body.filename, body._id);
                    // if (exists) {
                    //     body.filename = body.filename + '_renamed_' + new Date();
                    // await storage.redis.appendToAttachmentList(request_info, body.filename, body._id);

                    // } else {
                    logger.debug('date --- uA getpost', new Date())
                    
                    // }
                    var file_path = `${body._id}/attachments/${body.filename}`;

                    var post_info = await storage.mongoDB.getPostData(request_info, {
                        _id: body._id
                    });
                    if (lodash.isEmpty(post_info)) {
                        await storage.redis.createFileList(request_info, body._id, file_path);
                        logger.debug('date --- uA aftercreatefilelist', new Date())
                        
                    } else {
                        // post_info.attachments.push(file_path);
                        post_info = {
                            _id: post_info._id,
                            $addToSet: {
                                attachments: file_path
                            }
                        }
                        console.log(' created post ------------------')
                        await storage.mongoDB.createPostData(request_info, post_info);
                    }
                    await storage.aws.streamFilesToS3(request_info, process.env.BLOG_ATTACHMENT_BUCKET, file, file_path);
                    logger.info(S, 'success', body);
                    return resolve('success');


                });
            } catch (err) {
                logger.error(S, err, body);
                return reject(err);
            }
        });
    },
    deletePost: function (request_info, postId) {
        const S = request_info + M + '[deletePost]';
        return new Promise(async (resolve, reject) => {
            try {
                var post_info = await storage.mongoDB.getPostData(request_info, {
                    _id: postId
                });
                if(lodash.isEmpty(post_info)){
                    return reject('Invalid PostId');
                }else{
                async.parallel([
                    (cbInner) => {
                        process.nextTick(async () => {
                            await storage.mongoDB.deletePostData(request_info, {
                                _id: postId
                            });
                            cbInner(null);
                        });
                    },
                    (cbInner) => {
                        process.nextTick(async () => {
                            await storage.aws.deleteFromS3(request_info, process.env.BLOG_ATTACHMENT_BUCKET, postId);
                            await storage.redis.clearFileList(request_info, postId);
                            cbInner(null);
                        });
                    }
                ], (err) => {
                    if (err) {
                        logger.error(S, postId, err);
                        if (err.name == 'DBCastError') {
                            return reject('Invalid PostId');
                        } else {
                            return reject(err);
                        }
                    } else {
                        return resolve('success');
                    }
                });
            }
            } catch (err) {
                logger.error(S, postId, err);
                return reject(err);
            }
        });
    },
    likePost: function (params){
        logger.info("IN --> blogic/post/likePost");
        return new Promise(async (resolve, reject) => {
            try {
                var postId = params.postId
                logger.debug("postId:", postId)
                var data = await storage.mongoDB.updatePostInDB(postId, {$inc: {likes:1}});
                resolve(data);
            } catch (err) {
                logger.error("Could not like post:", err)
                reject(err)
            }
        });
    },

    dislikePost: function (params){
        logger.info("IN --> blogic/post/dislikePost");
        return new Promise(async (resolve, reject) => {
            try {
                var postId = params.postId
                logger.debug("postId:", postId)
                var data = await storage.mongoDB.updatePostInDB(postId, {$inc: {unlikes:1}});
                resolve(data);
            } catch (err) {
                logger.error("Could not dislike post:", err)
                reject(err)
            }
        });
    },
    followPost: function (params, user){
        logger.info("IN --> blogic/post/followPost");
        return new Promise(async (resolve, reject) => {
            try {
                var postId = params.postId
                logger.debug("postId:", postId)
                var data = await storage.mongoDB.updatePostInDB(postId, {$push: {followers: user.id}});
                resolve(data);
            } catch (err) {
                logger.error("Could not follow post:", err)
                reject(err)
            }
        });
    },

    unfollowPost: function (params, user){
        logger.info("IN --> blogic/post/unfollowPost");
        return new Promise(async (resolve, reject) => {
            try {
                var postId = params.postId
                logger.debug("postId:", postId)
                var data = await storage.mongoDB.updatePostInDB(postId, {$pull: {followers: user.id}});
                resolve(data);
            } catch (err) {
                logger.error("Could not unfollow post:", err)
                reject(err)
            }
        });
    }
}


module.exports = {
    post: post
}