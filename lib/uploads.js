var Busboy = require('busboy');
var logger = require('../utils/logger').getLogger('');

const M = '[Lib][Upload]';


var url_map = {
    '/api/v1/collaboration/post': 'all',
    '/api/v1/collaboration/post/attachment': 'file'
};

function upload(req, validateFn, fileHandler, fieldHandler, errHandler, done) {
    const S = req.request_info + M + '[handler]';
    var body = {
        files_attached: Number(req.headers['x-files-attached']),
        upload_mode: url_map[req.originalUrl],
        _id: req.headers['x-post-id'],
        files_uploaded: false,
        fields_uploaded: false,
        owner: req.user._id,
        tags: [],
        attachments: []
    };
    if (body.files_attached) {
        body.upload_completed = 0;
    }

    var already_started = false;

    logger.debug('entered busboy, started upload', body)
    var busboy = new Busboy({
        headers: req.headers
    });

    var error = (body.upload_mode == 'all') ? 'Please Provide the Required Fields' : null;
    var final_result;
    if (!body.files_attached && (body.upload_mode == 'file')) {
        done('Please Provide Non-Zero File Attach Header', {
            postid: req.headers['x-post-id']
        });
    } else {
        if (body._id) {
            if (body.files_attached) {

                busboy.on('file', async function (fieldname, file, filename) {
                    logger.info('date --- file', new Date());
                    try {
                        body.filename = filename.replace(/[(){}\s]/g, '');
                        body.attachments.push(`${body._id}/attachments/${body.filename}`);

                        await fileHandler(req, file, body);

                        final_result = result;
                        body.upload_completed += 1;
                        if (body.upload_completed == body.files_attached) {
                            body.files_uploaded = true;
                            if ((body.upload_mode == 'file') || body.fields_uploaded) {
                                logger.debug('date --- filehandler', new Date());
                                busboy.emit('finish');
                                already_started = true;
                            }
                        }
                    } catch (err) {
                        error = err;
                        busboy.emit('finish');
                    }

                });
            }
            if (body.upload_mode != 'file') {
                busboy.on('field', async function (fieldname, val) {
                    logger.info('date --- field', new Date());

                    if (fieldname == 'tag_length') {
                        body.tag_length = Number(val);
                    } else if (fieldname == 'tags') {
                        try {
                            var tags = val.split(',');
                            tags = tags.filter(tag => tag);
                            body.tags = tags;
                            
                        } catch (err) {
                            error = 'Invalid Tags Field';
                            busboy.emit('finish');
                        }
                    } else {
                        body[fieldname] = val;
                    }
                    logger.info(S, 'Field [' + fieldname + ']: value: ' + val);

                    try {
                        if (validateFn(req, body) && !already_started) {
                            already_started = true;
                            error = null;
                            body.fields_uploaded = true;
                            if (!body.files_attached || body.files_uploaded) {
                                logger.debug('date --- fieldhandler', new Date());

                                busboy.emit('finish');

                            }

                        }
                    } catch (err) {
                        error = err;
                        busboy.emit('finish');
                    }

                });
            }

            busboy.on('finish', async () => {
                logger.debug('date --- fins', new Date(), body)
                busboy.removeAllListeners('field');
                busboy.removeAllListeners('file');
                if ((body.upload_mode == 'all') && !error) {
                    try {
                        if (body.tags.length != body.tag_length) {
                            error = 'Invalid Tags Field';
                        }else{
                            logger.debug('date --- fins2', new Date(), body);

                            await fieldHandler(req, body);
                        }
                    } catch (err) {
                        error = err;
                    }

                }
                if (error) {
                    done(error, {
                        postid: req.headers['x-post-id']
                    })
                } else {
                    done(null, {
                        postid: req.headers['x-post-id']
                    });
                }
            });
            req.pipe(busboy);
        } else {
            done('Missing Post ID Header', {
                postid: req.headers['x-post-id']
            });
        }
    }
}

module.exports = upload;