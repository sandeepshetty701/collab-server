'use strict';

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/xyz-schema', {useMongoClient: true});
require('../models/organisationModel');
require('../models/userModel');
require('../models/userPermissionModel');
require('../models/userTokenModel');
require('../models/postModel');

var ObjectID = require('mongodb').ObjectID;
var casual = require('casual');
var async = require('async');
var lodash = require('lodash');
var bcrypt = require('bcrypt-nodejs');
var User = mongoose.model('User');
var Organisation = mongoose.model('Organisation');
var UserPermission = mongoose.model('UserPermission');
var UserToken = mongoose.model('UserToken');
var Post = mongoose.model('Post');


var TokenGenerator = require('uuid-token-generator');
// token generator instance to generate random token to use as o-auth-token
var tokgen = new TokenGenerator(256, TokenGenerator.BASE62);

var email_list = [];
/**
 *   retrieve the Construction Units associated to the project
 */

var create_organisation = function(cb) {
    return new Promise( (resolve, reject) => {
        var orgObj = {
            _id: new ObjectID(),
            name: casual.name,
            email: casual.email,
            mobile: casual.numerify('9#########')
        };

        var organisation = new Organisation(orgObj);

        organisation.save(function(err) {
            if(err){
                return reject(err);
            }else{
                return resolve(orgObj);
            }
        })
    });

}

var create_user = function(orgObj, cb) {
    var userObj = {
        _id: new ObjectID(),
        organisationId: orgObj._id,
        name: casual.name,
        email: casual.email.toLowerCase(),
        fullName: casual.full_name,
        password: casual.password,
        mobile: casual.numerify('9#########')
    };
    console.log(userObj.email, userObj.password);
    if(email_list.length){
        userObj.created_by = email_list[0]._id;
    }
    email_list.push(userObj);
    userObj.password = bcrypt.hashSync(userObj.password, bcrypt.genSaltSync(10));

    var user = new User(userObj);

    user.save(function(err) {
        return cb(err, userObj);
    });
}

var create_permission = function(userObj, cb) {
    var permObj = {
        _id: new ObjectID(),
        user_id: userObj._id
    };

    var user_permission = new UserPermission(permObj);

    user_permission.save(function(err) {
        return cb(err, userObj);
    });
}

var create_token = function(userObj, cb){
    var tokenObj = {
        _id: new ObjectID(),
        user: userObj._id,
        token: tokgen.generate()
    };

    var user_token = new UserToken(tokenObj);
    userObj.token = user_token.token;
    email_list.push(userObj);

    user_token.save(function(err) {
        return cb(err, tokenObj);
    });
}

var create_post = function(userObj, cb) {
    var postObj = {
        _id: new ObjectID(),
        owner: userObj._id,
        project: mongoose.Types.ObjectId("5abf81b88c0d217615448d01"),
        post_type: casual.word,
        title: casual.title,
        body: casual.sentence,
        likes: casual.integer(0, 100)
    };

    var post = new Post(postObj);

    post.save(function(err) {
        return cb(err, postObj);
    });
}

var run_migration = async () => {

    try{
        var orgObj = await create_organisation();

        async.each(lodash.range(20), function(i, cb){

            async.waterfall([ function (cbInner){
                // create_organisation(cbInner);
            // }, function (orgObj, cbInner){
                create_user(orgObj, cbInner);
            }, function (userObj, cbInner){
                // console.log(userObj);
                create_permission(userObj, cbInner);
            }, function (userObj, cbInner){
                create_token(userObj, cbInner);
            }, function (userObj, cbInner){
                create_post(userObj, cbInner);
            }], function(err){
                if(err)console.log(err);
                cb(null);
            });

        }, function(err){
            process.exit(0);
        });

    }catch(err){
        process.exit(0);
    }
};

run_migration();

module.exports = {};

