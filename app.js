const https = require('https');
const http = require('http');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const path = require('path');
const fs = require('fs');
const rfs = require('rotating-file-stream');

const express = require('express');
const app = express();

process.env.TZ = 'UTC';
require('dotenv').config();

//Import Routes
var index = require('./routes/index');

app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: false
    })
);

//Logging setup
var logger = require("./utils/logger").getLogger("APP");

var logDirectory = path.join(__dirname, 'logs')
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory)

var accessLogStream = rfs('access.log', {
    interval: '1d', // rotate daily
    path: logDirectory
})
var errorLogStream = rfs('error.log', {
    interval: '1d', // rotate daily
    path: logDirectory
})

// log only 4xx and 5xx responses to console
app.use(morgan('short', {
    skip: function (req, res) {
        return res.statusCode < 400;
    },
    stream: errorLogStream
}))

app.use(morgan('short', {
    skip: function (req, res) {
        return res.statusCode > 400;
    },
    stream: accessLogStream
}));

//Cors
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    var allowedOrigins = [
        "https://3rdi.xyz",
        "https://3rdi.staging.xyzinnotech.com",
        "https://platform-3rdi.firebaseapp.com",
        "http://localhost:4200",
        "https://amaravatiportal.apcrda.org"
    ];

    var origin = req.headers.origin;
    if (allowedOrigins.indexOf(origin) > -1) {
        res.setHeader("Access-Control-Allow-Origin", origin);
    }
    
    var method_requested = req.headers['access-control-request-method']||'POST';
    logger.debug("[app] origin: %s, method_requested: %s", origin, method_requested)
    // Request methods you wish to allow
    res.setHeader(
        "Access-Control-Allow-Methods",
        method_requested
    );

    // Request methods you wish to allow
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, OPTIONS, PUT, PATCH, DELETE"
    );

    // Request headers you wish to allow
    res.setHeader(
        "Access-Control-Allow-Headers",
        "X-Requested-With,Content-Type,X-Auth-Token,X-User-Id,X-Post-Id,X-Request-Id"
    );

    // Set to true if you need the website to
    // include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader("Access-Control-Allow-Credentials", true);

    // intercept OPTIONS method
    if ("OPTIONS" == req.method) {
        res.sendStatus(200);
    } else {
        // Pass to next layer of middleware
        next();
    }
});

process
    .on('unhandledRejection', (reason, p) => {
        logger.error(reason, 'Unhandled Rejection at Promise', p);
    })
    .on('uncaughtException', err => {
        logger.error(err, 'Uncaught Exception thrown');
    });

app.use(express.json({limit: '200mb'}));
app.use(express.urlencoded({ limit: '200mb', extended: true }));

//Use routes
app.use('/api/v1/collaboration', index);

app.all('*', function (req, res) {
    res.status(400)
    res.send('Invalid route');
});

let server;
if (process.env.SERVER == 'LOCAL') {
    logger.info('APP to serve https requests.')
    server = https.createServer(app);
} else {
    server = http.createServer(app);
    logger.info('APP to serve http requests.')
}

server.listen(process.env.PORT);
server.on('listening', () => {
    logger.debug("Collaboration server started on port: ", process.env.PORT)
});
