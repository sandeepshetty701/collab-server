'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
  // projectSchema = require('../model/projectModel').schema;

var UserPermissionSchema = new Schema({

  user_id: { //  object Id of user who got  this permissions
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },

  global_perm: {
    type: Object,
    default: {
        is_admin: false
    }
  },

  project_perm: {
      type:Array,
      default: []
  }

}, { timestamps: true });

module.exports = mongoose.model('UserPermission', UserPermissionSchema);