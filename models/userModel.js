'use strict';

var mongoose = require('mongoose'),
    bcrypt = require('bcrypt-nodejs'),
    Schema = mongoose.Schema;

var UserSchema = new Schema({

    organisationId: { // Organisation to where the user belongs
        type: Schema.Types.ObjectId,
        ref: 'Organisation',
        required: false
    },

    fullName: { // full name of the user
        type: String,
        trim: true
    },

    email: { // email of the user
        type: String,
        lowercase: true,
        trim: true,
        required: true
    },

    password: { // password hashed using bcryptjs
        type: String,
        required: true
    },


    mobile: { // mobile number of the user. min and max as per indian mobile numbers
        type: Number,
        min: 1000000000,
        max: 9999999999,
        required: false
    },

    countryCode: { // mobile country code
        type: String,
        default: '+91',
        required: false
    },

    is_active: {   //  whether user is active
        type: Boolean,
        default: true
    },

    emailValidated: {   //  whether email is validated
        type: Boolean,
        default: false
    },

    created_by: { //
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: false

    },

    avatar: {
        type: String, // user for user profile image
        required: false
    },
    is_admin: {// admin or not
        type: Boolean,
        default: false
    },

    permissions: {
        type: Object,
        default: {
            is_admin: false,
            can_make_admin: false
        }
    }

}, { timestamps: true });

UserSchema.index({ email: 1, organisationId: 1 }, { unique: true });
/**
 *   utils method to compare given plain text password with the hashed password
 */
UserSchema.methods.comparePassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('User', UserSchema);
