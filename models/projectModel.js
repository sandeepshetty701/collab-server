'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ProjectSchema = new Schema({
  slug: {
    type: String,
    trim: true
  },
  name: { //  name of the project

    type: String,
    trim: true,
    unique: true,
    required: true
  },

  description: { //  few lines description about the project

    type: String,
    trim: true
  },

  organisationId: {
    type: Schema.Types.ObjectId,
    ref: 'Organisation'
    // required: true
  },

  plannedstart: {
    type: Date,
    trim: true
  },

  planned_finish_date: {
    type: Date,
    trim: true
  },
  
  actual_start_date: {
    type: Date,
    trim: true
  },

  actual_finish_date: {
    type: Date,
    trim: true
  },

  duration: {
    type: Date,
    trim: true
  },

  manpower: {
    type: String,
    trim: true
  },

  scope: {
    quantity: {
      type: Number
    },
    unit: {
      type: String
    }
  },
  
  plannedcost: {
    type: {
      type: String,
    },
    value: {
      type: Number
    },
    code: {
      type: String
    }
  },

  name: { //  name of the project

      type: String,
      trim: true,
      unique: true,
      required: true
  },

  owner: {
    type: Schema.Types.ObjectId,
    ref: 'User'
    // required: true
  },

  

    projectType: {  //  Type of project. Currently suppourted values construction-management, MAUD, smart-city

        type: String,
        trim: true
    },

    created_by: { //  object Id of user who created this project

        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },

    parent: { //    parent project for this project

        type: Schema.Types.ObjectId,
        ref: 'Project'
    },

    projects: [{ //     all the subprojects for this project

        type: Schema.Types.ObjectId,
        ref: 'Project'
    }],

    thumbnail: String, // thumbnail of the project

    users: [{

        user: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        },
        role: {
            type: Schema.Types.ObjectId,
            ref: 'Role'
        }

    }], // Object ids of all the users associated to this project

    orthomosaics: [{

        type: Schema.Types.ObjectId,
        ref: 'Tileset'

    }], // Object ids of all the orthomosaics associated to this project

    components: [{

        type: Schema.Types.ObjectId,
        ref: 'Component'

    }], // Object ids of all the components associated to this project

    classifications: [{

        type: Schema.Types.ObjectId,
        ref: 'Classification'

    }] // Object ids of all the classifications associated to this project

}, { timestamps: true });

module.exports = mongoose.model('Project', ProjectSchema);
