'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var OrganisationSchema = new Schema({

    name: { // name of the organisation
        type: String,
        trim: true,
        required: true
    },

    email: { // email of the organisation
        type: String,
        // unique: true,
        lowercase: true,
        trim: true,
        required: true
    },


    mobile: { 
    /** mobile number of the contact person of organisation.
         min and max as per indian mobile numbers
    */
        type: Number,
        min: 1000000000,
        max: 9999999999,
        required: true,
    },

    countryCode: { // mobile country code
        type: String,
        default: '+91'
    },

    landlineNumber: { // landline number
        type: String
    },

    is_active: {   //  whether organisation is active
        type: Boolean,
        default: true
    },

}, { timestamps: true });

 
module.exports = mongoose.model('Organisation', OrganisationSchema);