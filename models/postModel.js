'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PostSchema = new Schema({

    parent: { 
        type: Schema.Types.ObjectId,
        ref: 'Post',
        required: false
    },

    owner: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },

    project: {
        type: Schema.Types.ObjectId,
        ref: 'Project',
        required: true
    },

    post_type: {
        type: String,
        required: true
    },

    title: {
        type: String,
        required: true
    },

    text_content: {
        type: String,
        required: true
    },

    visibility: {
        type: String,
        required: true
    },

    followers: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],

    selectedUsers: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],

    likes: {
        type: Number,
        required: false,
        default: 0
    },
    unlikes: {
        type: Number,
        required: false,
        default: 0
    },

    attachments: [
        { type: String }
    ],

    tags: [{ 
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],

    replies: [{
        type: Schema.Types.ObjectId,
        ref: 'Post'
    }]

}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }

});

module.exports = mongoose.model('Post', PostSchema);